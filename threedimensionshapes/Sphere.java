package threedimensionshapes;


public class Sphere extends ThreeDimensionalShape {
    private double r;

    public Sphere(double r) {
        this.r = r;
    }

    @Override
    public double area() {
        return 4 * Math.PI * r * r;
    }

    @Override
    public double perimeter() {
        return 4 / 3 * Math.PI * r * r * r;
    }

    @Override
    public double volume() {
        return 4 / 3 * Math.PI * r * r;
    }
}

