package threedimensionshapes;

import shape.Shape;

public abstract class ThreeDimensionalShape extends Shape {

    public abstract double area();

    public abstract double perimeter();

    public abstract double volume();

}

