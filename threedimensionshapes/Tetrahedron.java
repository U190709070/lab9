package threedimensionshapes;


public class Tetrahedron extends ThreeDimensionalShape {
    private double side;

    public Tetrahedron(double side) {
        this.side = side;
    }

    @Override
    public double area() {
        return Math.sqrt(3) * side * side * side;
    }

    @Override
    public double perimeter() {
        return 6 * side;
    }

    @Override
    public double volume() {
        return side * side * side / (6 * Math.sqrt(2));
    }
}

