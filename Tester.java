import threedimensionshapes.*;
import twodimensionshapes.*;
import shape.Shape;

import java.io.*;
import java.util.ArrayList;

public class Tester {
    public static void main(String[] args) throws IOException {
        File file = new File("instruction.txt");
        FileReader fileReader = new FileReader(file);
        String line;
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        while ((line = bufferedReader.readLine())!= null){
            System.out.println(line);
        }
        ArrayList<Shape> shapes = new ArrayList<Shape>();
        Shape C = new Circle(5);
        Shape S = new Square(5);
        Shape T = new Triangle(3, 4, 5, 4.7);
        Shape SP = new Sphere(7);
        Shape CU = new Cube(3);
        Shape TE = new Tetrahedron(2.5);
        shapes.add(C);
        shapes.add(S);
        shapes.add(T);
        shapes.add(SP);
        shapes.add(CU);
        shapes.add(TE);
        double TA = 0;
        double TP = 0;
        double TV = 0;
        for (Shape shape : shapes){
            TA += shape.area();
        }
        for (Shape shape : shapes){
            TP += shape.perimeter();
        }

        for (Shape shape : shapes){
            if (shape instanceof ThreeDimensionalShape){
                TV += shape.volume();
            }
        }
        System.out.println("Total Area = " + TA + "\nTotal Perimeter = " + TP + "\nTotal Volume = " + TV);
    }
}
