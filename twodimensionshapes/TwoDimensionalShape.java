package twodimensionshapes;


import shape.Shape;

public abstract class TwoDİmensionalShape extends Shape {

    public abstract double area();

    public abstract double perimeter();

}
